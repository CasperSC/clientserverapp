﻿using System;
using System.Net;

namespace CommunicationLayer
{
    public static class Const
    {
        static Const()
        {
            var uriBuilder1 = new UriBuilder("net.tcp", Dns.GetHostName(), 8080, "MainService");
            MainServiceAddress = uriBuilder1.ToString();

            var uriBuilder2 = new UriBuilder("net.tcp", Dns.GetHostName(), 8090, "ProxyService");
            ProxyAddress = uriBuilder2.ToString();
        }

        public static string MainServiceAddress { get; private set; }

        public static string ProxyAddress { get; private set; }
    }
}

﻿using System.ServiceModel;

namespace CommunicationLayer.Interfaces
{
    [ServiceContract(SessionMode = SessionMode.Allowed, CallbackContract = typeof(IDataServiceCallback))]
    public interface IDataService
    {
        [OperationContract]
        string GetFile(string fileName);
    }
}

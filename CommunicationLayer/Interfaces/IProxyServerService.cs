﻿using System.IO;
using System.ServiceModel;

namespace CommunicationLayer
{
    [ServiceContract(SessionMode = SessionMode.Allowed, CallbackContract = typeof(IProxyServerServiceCallback))]
    public interface IProxyServerService
    {
        [OperationContract]
        string GetFile(string file);
    }
}

using System.IO;
using System.ServiceModel;

namespace CommunicationLayer
{
    [ServiceContract]
    public interface IProxyServerServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void SendMessage(string message);

        [OperationContract]
        void SendData(Stream stream);
    }
}
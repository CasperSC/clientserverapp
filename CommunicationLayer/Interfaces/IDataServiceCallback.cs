using System.ServiceModel;

namespace CommunicationLayer.Interfaces
{
    [ServiceContract]
    public interface IDataServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void SendMessage(string message);
    }
}
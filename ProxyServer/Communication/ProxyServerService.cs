﻿using System;
using System.ServiceModel;
using System.Threading;
using CommunicationLayer;
using CommunicationLayer.Interfaces;

namespace ProxyServer.Communication
{
    [ServiceBehavior(
    ConcurrencyMode = ConcurrencyMode.Multiple,
    InstanceContextMode = InstanceContextMode.PerSession,
    UseSynchronizationContext = false)]
    public class ProxyServerService : IProxyServerService
    {
        private readonly string[] _files;
        private readonly IProxyServerServiceCallback _callback;
        private IDataService _service;

        public ProxyServerService()
        {
            _files = new[] { "file1", "file2" };
            _callback = OperationContext.Current.GetCallbackChannel<IProxyServerServiceCallback>();
        }

        public string GetFile(string fileName)
        {
            Console.WriteLine("Ищем файл: {0}.", fileName);
            Thread.Sleep(1000);

            foreach (string file in _files)
            {
                if (file == fileName)
                {
                    return file;
                }
            }

            Console.WriteLine("Файл не найден. Ищем на внутреннем сервере.");
            //Отсылаем сообщение клиенту через колбек.
            _callback.SendMessage("Файл на основном сервере не найден, продолжаем поиск на внутреннем сервере");

            Thread.Sleep(1000);
            if (_service == null)
            {
                _service = InitServiceInternal(new DataServiceCallback());
                
            }

            string text = _service.GetFile(fileName);
            if (text != null) //Понятно, что это не файл, не суть.
            {
                Console.WriteLine("Файл от сервера получен, передаём клиенту.");
            }
            return text;
        }

        protected static IDataService InitServiceInternal(IDataServiceCallback callback)
        {
            EndpointAddress endpoint = new EndpointAddress(new Uri(Const.MainServiceAddress));
            var binding = new NetTcpBinding(SecurityMode.None);
            var service = DuplexChannelFactory<IDataService>.CreateChannel(callback, binding, endpoint);
#if DEBUG
            binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
            binding.SendTimeout = TimeSpan.FromMinutes(10);
#endif
            return service;
        }
    }
}

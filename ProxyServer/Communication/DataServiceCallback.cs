﻿using System;
using System.ServiceModel;
using CommunicationLayer.Interfaces;
using Utils;

namespace ProxyServer.Communication
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    internal class DataServiceCallback : ConsoleLoggerBase, IDataServiceCallback
    {
        public DataServiceCallback(string from = "Server: ", ConsoleColor fromColor = ConsoleColor.White)
            : base(from, fromColor)
        {
        }

        #region Implementation of IDataServiceCallback

        public void SendMessage(string message)
        {
            WriteToConsole(message);
        }

        #endregion


    }
}
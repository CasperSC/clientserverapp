﻿using System;
using System.ServiceModel;
using CommunicationLayer;
using ProxyServer.Communication;

namespace ProxyServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Proxy Server > Server"; // > - подключен к

            ServiceHost host = CreateServiceHost<IProxyServerService, ProxyServerService>(Const.ProxyAddress);
            host.Open();
            Console.WriteLine("Сервис запущен");

            Console.ReadKey();
        }

        private static ServiceHost CreateServiceHost<TContractType, TImplementationType>(string address)
            where TContractType : class
            where TImplementationType : class
        {
            ServiceHost host = new ServiceHost(typeof(TImplementationType));
            var binding = new NetTcpBinding(SecurityMode.None);
#if DEBUG
            binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
            binding.SendTimeout = TimeSpan.FromMinutes(10);
#endif
            host.AddServiceEndpoint(typeof(TContractType), binding, address);

            return host;
        }
    }
}

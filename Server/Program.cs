﻿using System;
using System.ServiceModel;
using CommunicationLayer;
using CommunicationLayer.Interfaces;
using Server.Communication;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Server";

            ServiceHost host = CreateServiceHost<IDataService, DataService>(Const.MainServiceAddress);
            host.Open();
            Console.WriteLine("Сервис запущен");

            Console.ReadKey();
        }

        private static ServiceHost CreateServiceHost<TContractType, TImplementationType>(string address)
            where TContractType : class
            where TImplementationType : class
        {
            ServiceHost host = new ServiceHost(typeof(TImplementationType));
            var binding = new NetTcpBinding(SecurityMode.None);
#if DEBUG
            binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
            binding.SendTimeout = TimeSpan.FromMinutes(10);
#endif
            host.AddServiceEndpoint(typeof(TContractType), binding, address);

            return host;
        }
    }
}

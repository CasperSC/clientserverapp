﻿using System;
using System.ServiceModel;
using System.Threading;
using CommunicationLayer;
using CommunicationLayer.Interfaces;

namespace Server.Communication
{
    [ServiceBehavior(
        ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode = InstanceContextMode.PerSession,
        UseSynchronizationContext = false)]
    internal class DataService : IDataService
    {
        private readonly IDataServiceCallback _callback;

        public DataService()
        {
            _callback = OperationContext.Current.GetCallbackChannel<IDataServiceCallback>();
        }

        public string GetFile(string fileName)
        {
            Console.WriteLine("Ищем файл: {0}.", fileName);
            _callback.SendMessage(string.Format("Внутренний сервер начал поиск файла: {0}", fileName));
            Thread.Sleep(1000);
            Console.WriteLine("Файл найден. Возвращаем результат.");

            return "Файл из основного сервера";
        }
    }
}

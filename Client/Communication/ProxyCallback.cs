﻿using System;
using System.IO;
using System.ServiceModel;
using CommunicationLayer;
using Utils;

namespace Client.Communication
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    public class ProxyCallback : ConsoleLoggerBase, IProxyServerServiceCallback
    {
        public ProxyCallback(string from = "ProxyServer: ", ConsoleColor fromColor = ConsoleColor.White)
            : base(from, fromColor)
        {
        }

        #region Implementation of IProxyServerServiceCallback

        public void SendMessage(string message)
        {
            WriteToConsole(message);
        }

        public void SendData(Stream stream)
        {
            WriteToConsole("Получен файл");
        }

        #endregion
    }
}
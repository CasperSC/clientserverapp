﻿using System;
using System.ServiceModel;
using Client.Communication;
using CommunicationLayer;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Client > Proxy Server"; // > - подключен к

            try
            {
                var callback = new ProxyCallback();
                IProxyServerService service = InitServiceInternal(callback);
                Console.WriteLine("Клиент подключен к ProxyServer");
                Console.WriteLine("Нажмите любую клавишу, чтобы начать");
                Console.ReadKey();
                Console.WriteLine("Запрашиваем файл у Proxy Server");
                string responce = service.GetFile("file3");
                Console.Write("Получили ответ от сервиса: ");
                Console.WriteLine("\"{0}\"", responce);
            }
            catch (FaultException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }

        protected static IProxyServerService InitServiceInternal(IProxyServerServiceCallback callback)
        {
            EndpointAddress endpoint = new EndpointAddress(new Uri(Const.ProxyAddress));
            var binding = new NetTcpBinding(SecurityMode.None);
            var service = DuplexChannelFactory<IProxyServerService>.CreateChannel(callback, binding, endpoint);
#if DEBUG
            binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
            binding.SendTimeout = TimeSpan.FromMinutes(10);
#endif
            return service;
        }
    }
}

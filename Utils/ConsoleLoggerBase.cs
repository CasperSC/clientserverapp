﻿using System;

namespace Utils
{
    public abstract class ConsoleLoggerBase
    {
        protected ConsoleLoggerBase(string from, ConsoleColor fromColor)
        {
            From = @from;
            FromColor = fromColor;
        }

        protected void WriteToConsole(string text)
        {
            ConsoleHelper.Write(From, FromColor);
            Console.WriteLine(text);
        }

        /// <summary>
        /// От кого
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Цвет текста сообщения "От кого".
        /// </summary>
        public ConsoleColor FromColor  { get; set; }
    }
}
